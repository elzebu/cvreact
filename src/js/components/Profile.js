import React from 'react';
import FontIcon from 'react-md/lib/FontIcons';

// TODO : à changer
import profile from '../../images/profile.jpg';

const Profile = props => {
    const profileObj = props.profileData;
    const getLanguage = props.languageData.map(function(item, index) {
        return (<li>{item.language} : {item.fluency}</li>)
    });

    const getNetwork = profileObj.profiles.map(function(item, index) {
        return (<li><a href={item.url}>
                      <img src={process.env.PUBLIC_URL +'images/'+ item.network + '.svg'} alt={item.network} />
                    </a>
                </li>)
    });

    return  <div>
                <div className="profileImg"><img className="img-circle center-block" src={profile} alt="" /></div>
                <div className="detail">
                  <h1 className="md-text-center">{profileObj.name}</h1>
                  <div className="md-title md-text-center md-divider-border md-divider-border--below">{profileObj.label}</div>
                  <ul className="list-unstyled contact-links text-center">
                    <li><FontIcon>home</FontIcon>{profileObj.location.address}, {profileObj.location.postalCode}, {profileObj.location.city}</li>
                    <li><FontIcon>phonelink_ring</FontIcon>{profileObj.phone}</li>
                    <li><FontIcon>mail_outline</FontIcon><a href={`mailto:${profileObj.email}`}>{profileObj.email}</a></li>
                  </ul>
                  <div className="divider"></div>
                  <ul className="profileLinks list-inline text-center">
                    {getNetwork}
                  </ul>
                  <div className="language">
                      <ul>
                      {getLanguage}
                      </ul>
                  </div>
                </div>
            </div>
};

export default Profile;
