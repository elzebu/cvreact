import React from 'react';
import FontIcon from 'react-md/lib/FontIcons';

const Education = props => {
    const getEducation = props.educationData.map(function(item, index) {
      const startdate = item.startDate;
      const enddate = (item.endDate !== '')? ' - ' + item.endDate:'';
      return (
          <div key={index}>
            <h3>{item.area} - <span className="institution">{item.institution}</span></h3>
            <p>{item.description}</p>
            <p className="date">{startdate} {enddate}</p>
          </div>
        )
    });

    return (
      <section className="education box">
        <h2 className="md-text-uppercase"><FontIcon className="">work</FontIcon> Formation</h2>
        {getEducation}
      </section>
    )
};

export default Education;
