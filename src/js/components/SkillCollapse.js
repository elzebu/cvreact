import React, { PureComponent } from 'react';
import Button from 'react-md/lib/Buttons/Button';
import Collapse from 'react-md/lib/Helpers/Collapse';

export default class SkillCollapse extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { 
      collapsed: (this.props.collapsed)?true:false,
      className: (this.props.collapsed)?'fa fa-angle-down':'fa fa-angle-up'
    };
    this._toggleCollapse = this._toggleCollapse.bind(this);
  }

  _toggleCollapse() {
    let className;
    if(this.state.collapsed) {
      className = 'fa fa-angle-up';
    } else {
      className = 'fa fa-angle-down';
    }
    this.setState({ 
      collapsed: !this.state.collapsed,
      className: className
    });
  }

  render() {
    return (
      <div className="skill-collapse">
        <h3><Button icon secondary onClick={this._toggleCollapse} className={this.state.className} /> {this.props.name}</h3>
        <p>{this.props.description}</p>
        <Collapse collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapse>
      </div>
    );
  }
}