import React from 'react';
import FontIcon from 'react-md/lib/FontIcons'

const About = props => {
    return (
      <section className="about box">
        <h2 className="md-text-uppercase"><FontIcon>face</FontIcon> A propos</h2>
        <div>{props.aboutData}</div>
      </section>
    );
};

export default About;
