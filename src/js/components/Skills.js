import React from 'react';
import FontIcon from 'react-md/lib/FontIcons';

import SkillCollapse from './SkillCollapse';
import SkillItem from './SkillItem';

const Skills = props => {
    const getSkills = props.skillsData.map(function(item, index) {
      return (
      <SkillCollapse name={item.name} description={item.description} children={<SkillItem key={index} skillItemData={item}/>} />
        )
    });

  	return (
  	  <section className="skills box">
        <h2 className="md-text-uppercase"><FontIcon>show_chart</FontIcon> Compétences</h2>
        <div className="chip-list">{getSkills}</div>
      </section>
  	)
};

export default Skills;