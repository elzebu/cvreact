import React from 'react';
import WorkItem from './WorkItem';
import FontIcon from 'react-md/lib/FontIcons';

const Work = props => {
    const getWorkExperience = () => {
        const workItems = [];
        props.workData.forEach((item, index) => {
          workItems.push(<WorkItem key={index} workItemData={item}/>);
        })
        return workItems;
    }

    return (
      <section className="work box">
        <h2 className="md-text-uppercase"><FontIcon>business</FontIcon> Expérience professionnelle</h2>
        {getWorkExperience()}
      </section>
    );
};

export default Work;
